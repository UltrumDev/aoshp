# Welcome to the Android Open Source History Project (*AOSHP*)!

## More builds are available at https://sourceforge.net/projects/aoshp/

#### Our goal is to save generic AOSP builds, so that future generations would be able to see how android has developed through time!

The rules are simple:

1. Sync with android repository

2. Do **NOT** change any sources

3. Make generic/emulator builds *(eng + userdebug + user)*

4. ##### You have saved another Android version for the future!

